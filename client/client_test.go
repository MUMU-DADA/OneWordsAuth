package client

import (
	"log"
	"sync"
	"testing"
	"time"
)

func TestDoAuth(t *testing.T) {
	pass, signal := DoAuth(OWASettings{})
	if !pass {
		log.Fatal("验证失败")
		return
	}
	t.Log("单次验证成功")
	wg := &sync.WaitGroup{}
	wg.Add(2)
	for i := 0; i < 3; i++ {
		go func(signal <-chan bool) {
			select {
			case pass := <-signal:
				if !pass {
					log.Fatal("循环验证失败")
					return
				}
			}
			t.Log("循环验证成功")
			wg.Done()
		}(signal)
	}
	go func() {
		time.Sleep(time.Second * (time.Duration(default_TIME_OUT) + time.Duration(default_SLEEP_TIME)) * time.Duration(default_RETRY_TIMES))
		log.Fatal("循环验证超时")
		return
	}()
	wg.Wait()
	t.Log("循环验证成功")
}
