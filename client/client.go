package client

import (
	"bytes"
	"crypto/tls"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"time"

	DA "gitee.com/MUMU-DADA/OneWordsAuth/auth/DefaultAlgorithm"
)

const (
	default_SLEEP_TIME  uint64 = 15 // 验证间隔时间
	default_TIME_OUT    uint64 = 5  // 接口访问超时秒数
	default_RETRY_TIMES uint8  = 3  // 重试次数

	default_HOST  string = "https://127.0.0.1:18809"
	default_API_1 string = "/get_real_time_code"
	default_API_2 string = "/auth"
)

var (
	// ErrChan chan bool    = make(chan bool)
	HttpC *http.Client = &http.Client{
		Transport: &http.Transport{
			TLSClientConfig: &tls.Config{
				InsecureSkipVerify: true,
			},
		},
		Timeout: time.Duration(default_TIME_OUT) * time.Second,
	}
)

// 客户端类
type client struct {
	settings   OWASettings
	signalChan chan bool
}

// 初始化客户端对象
func (c *client) initClient() {
	if c.settings.HostAddr == "" {
		c.settings.HostAddr = default_HOST
	}
	if c.settings.RetryTimes == 0 {
		c.settings.RetryTimes = default_RETRY_TIMES
	}
	if c.settings.SleepTime == 0 {
		c.settings.SleepTime = default_SLEEP_TIME
	}
	c.signalChan = make(chan bool)
}

// 获取 单次验证信息 和 持续验证信息
func (c *client) getAuth() (bool, <-chan bool) {
	go c.tAuth()
	return c.doAuth(1), c.signalChan
}

// 进行持续验证
func (c *client) tAuth() {
	for {
		time.Sleep(time.Duration(c.settings.SleepTime) * time.Second)
		pass := c.doAuth(1)
		if !pass {
			c.signalChan <- false
		} else {
			c.signalChan <- true
		}
	}
}

// 访问验证接口进行单次验证
func (c *client) doAuth(times uint8) bool {
	if times > c.settings.RetryTimes {
		return false
	}
	nowTimes := times + 1
	// 获取访问接口获得salt
	resp, err := HttpC.Get(c.settings.HostAddr + default_API_1)
	if err != nil {
		return c.doAuth(nowTimes)
	}
	if resp.StatusCode != 200 {
		return c.doAuth(nowTimes)
	}
	bodyByte, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return c.doAuth(nowTimes)
	}
	salt := &DA.Salt{}
	if err := json.Unmarshal(bodyByte, salt); err != nil {
		return c.doAuth(nowTimes)
	}
	// 生成验证值
	rightStr := DA.MakeAuthBySalt(salt, time.Now().UTC().Format(DA.TIME_PARSE))
	// 装填返回结构体
	getAuth := &DA.GetAuth{
		Id:      salt.Id,
		AuthStr: rightStr,
	}
	getAuthByte, err := json.Marshal(getAuth)
	if err != nil {
		return c.doAuth(nowTimes)
	}
	resp, err = HttpC.Post(c.settings.HostAddr+default_API_2, "", bytes.NewReader(getAuthByte))
	if err != nil {
		return c.doAuth(nowTimes)
	}
	if resp.StatusCode != 200 {
		return c.doAuth(nowTimes)
	}
	return true
}

// 客户端设置信息
type OWASettings struct {
	HostAddr   string // 连接服务器地址 示例: https://127.0.0.1:18809
	SleepTime  uint64 // 接口访问超时秒数
	RetryTimes uint8  // 重试次数
}

// 持续验证直到失败退出
func CyclicCheackPass(chanIn <-chan bool) {
	for {
		select {
		case pass := <-chanIn:
			if !pass {
				panic(fmt.Errorf("程序结束: %v", pass))
			}
		}
	}
}

// 验证
// 连接服务器地址 示例: https://127.0.0.1:18809 如果输入空值的话
func DoAuth(settings OWASettings) (bool, <-chan bool) {
	c := client{
		settings: settings,
	}
	c.initClient()
	return c.getAuth()
}
