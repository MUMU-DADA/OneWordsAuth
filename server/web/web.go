package web

import (
	"log"
	"net/http"
	"time"

	"gitee.com/MUMU-DADA/OneWordsAuth/server/web/router"
	"gitee.com/MUMU-DADA/OneWordsAuth/utils/pem"
)

func InitWeb(addr string, schemes string) *http.Server {
	// 检查并生成公私钥
	if err := pem.CheckPemKey(); err != nil {
		log.Fatal(err)
	}
	s := &http.Server{
		Addr:         addr,
		ReadTimeout:  10 * time.Second,
		WriteTimeout: 10 * time.Second,
	}
	s.Handler = router.InitRouter(schemes)
	return s
}
