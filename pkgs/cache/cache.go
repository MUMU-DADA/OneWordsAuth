package cache

import (
	"time"

	"gitee.com/MUMU-DADA/OneWordsAuth/pkgs/logs"

	ch "github.com/faabiosr/cachego"
	redisCache "github.com/faabiosr/cachego/redis"
	defaultCache "github.com/faabiosr/cachego/sync"
	rd "gopkg.in/redis.v4"
)

const (
	defaultTimeout time.Duration = 10 * time.Minute
)

var (
	cacheTimeout  = 5
	RedisAddr     = "0.0.0.0:123"
	redisPassword = "123456"

	GlobalCache *Cache // 全局缓存
)

type Cache struct {
	c           ch.Cache
	defaultTime time.Duration
}

// 初始化全局缓存
func initCache(cache ch.Cache, time2 time.Duration) *Cache {
	return &Cache{
		c:           cache,
		defaultTime: time2,
	}
}

// Contains 检测缓存中是否存在对应的键值对
func (c *Cache) Contains(key string) bool {
	return c.c.Contains(key)
}

// Delete 删除缓存中的键值对
func (c *Cache) Delete(Key string) error {
	if err := c.c.Delete(Key); err != nil {
		logs.L.Error(err.Error())
		return err
	}
	return nil
}

// DeleteMulti 批量删除键值对
func (c *Cache) DeleteMulti(keys []string) error {
	allList := c.FetchMulti(keys)
	var deleteList = make(map[string]string, 0)
	for k, v := range allList {
		if err := c.Delete(v); err != nil {
			// 删除失败 回滚
			c.SaveMulti(deleteList)
			return err
		}
		deleteList[k] = v
	}
	return nil
}

// Fetch 获取缓存中的键值对
func (c *Cache) Fetch(key string) (string, error) {
	ans, err := c.c.Fetch(key)
	if err != nil {
		logs.L.Error(err.Error())
		return "", err
	}
	return ans, nil
}

// FetchMulti 批量获取缓存中的键值对
func (c *Cache) FetchMulti(keys []string) map[string]string {
	return c.c.FetchMulti(keys)
}

// Flush 清空缓存
func (c *Cache) Flush() error {
	if err := c.c.Flush(); err != nil {
		logs.L.Error(err.Error())
		return err
	}
	return nil
}

// Save 保存键值对到缓存中
func (c *Cache) Save(key, value string) error {
	if err := c.c.Save(key, value, c.defaultTime); err != nil {
		logs.L.Error(err.Error())
		return err
	}
	return nil
}

// SaveByTime 按照自定义时间保存键值对到缓存中
func (c *Cache) SaveByTime(key, value string, lifeTime time.Duration) error {
	if err := c.c.Save(key, value, lifeTime); err != nil {
		logs.L.Error(err.Error())
		return err
	}
	return nil
}

// SaveMulti 按照自定义时间批量保存键值对到缓存中
func (c *Cache) SaveMultiByTime(kvs map[string]string, lifeTime time.Duration) error {
	saveList := make([]string, 0)
	for k, v := range kvs {
		if err := c.SaveByTime(k, v, lifeTime); err != nil {
			// 批量保存失败 回滚操作
			c.DeleteMulti(saveList)
			return err
		}
		saveList = append(saveList, k)
	}
	return nil
}

// SaveMulti 批量保存键值对到缓存中
func (c *Cache) SaveMulti(kvs map[string]string) error {
	saveList := make([]string, 0)
	for k, v := range kvs {
		if err := c.Save(k, v); err != nil {
			// 批量保存失败 回滚操作
			c.DeleteMulti(saveList)
			return err
		}
		saveList = append(saveList, k)
	}
	return nil
}

// 缓存测试
func (c *Cache) checkCache() {
	logs.L.Info("测试缓存....")
	err := GlobalCache.Save("test", "1")
	_, err = GlobalCache.Fetch("test")
	if err != nil {
		logs.L.Error("缓存测试失败.... " + err.Error())
		panic("缓存测试失败.... " + err.Error())
	}
	logs.L.Info("缓存创建成功....")
}

// 创建一个redis缓存
func makeRedisCache() {
	logs.L.Info("启动redis缓存....")
	GlobalCache = initCache(
		redisCache.New(
			rd.NewClient(&rd.Options{
				Addr:     RedisAddr,
				Password: redisPassword,
			}),
		),
		time.Duration(cacheTimeout)*time.Second,
	)
	GlobalCache.checkCache()
}

// 创建一个默认基础内存缓存
func makeDefaultCache() {
	logs.L.Info("启动默认基础缓存....")
	GlobalCache = initCache(
		defaultCache.New(),
		time.Duration(cacheTimeout)*time.Second,
	)
	GlobalCache.checkCache()
}

// Init 初始化全局cache
func Init(cacheSwitch string) {
	switch cacheSwitch {
	case "redis":
		makeRedisCache()
	case "default":
		makeDefaultCache()
	default:
		makeDefaultCache()
	}
}
