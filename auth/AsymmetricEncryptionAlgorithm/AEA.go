package AEA

var (
	AllAEAEngineMap map[string]IAEA = make(map[string]IAEA) // 保存注册的所有对称加密算法引擎
)

// 通用对称加密算法接口
type IAEA interface{}

// 初始化并注册所有引擎
func init() {}
