package LHA

var (
	AllLHAEngineMap map[string]ILHA = make(map[string]ILHA) // 保存注册的所有线性散列算法引擎
)

// 通用线性散列算法接口
type ILHA interface{}

// 初始化并注册所有引擎
func init() {}
