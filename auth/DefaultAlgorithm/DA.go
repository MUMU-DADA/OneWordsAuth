package DA

import (
	"crypto/sha1"
	"encoding/json"
	"fmt"
	"net/http"
	"time"

	"gitee.com/MUMU-DADA/OneWordsAuth/pkgs/cache"
	"github.com/go-basic/uuid"
)

const (
	PROVATE_KEY string = "LmNVr22hVR3mZreR49vJj7bOWYoAqlPA3AirdNn5fDBEE5MMTSXhRvDJ5cxLQVKCJAQScPSsMCEo9lRgF12HwHDpOHd2QdslASkLRsfchWAF6QSMBdQ2fBE4hPAo1Ipi" // 定义好的通讯密钥
	TIME_PARSE  string = "2006-01-02 15:04"                                                                                                                 // 默认事件格式化格式

	SALT_TIMEOUT time.Duration = 5
)

// 默认加密引擎
type DefaultEngine struct{}

type Salt struct {
	Id   string `json:"salt_id"`
	Salt string `json:"salt"`
	Time string `json:"time"`
}

type GetAuth struct {
	Id      string `json:"salt_id"`
	AuthStr string `json:"auth"`
}

type AuthStatus struct {
	StatusCode int
	Msg        string
}

// 向缓存中保存生成的盐值
func saveSalt(s *Salt) error {
	temp := map[string]string{
		s.Id + "_Salt": s.Salt,
		s.Id + "_Time": s.Time,
	}
	return cache.GlobalCache.SaveMultiByTime(temp, time.Second*SALT_TIMEOUT)
}

// 向缓存中获取生成的盐值
func getSalt(id string) *Salt {
	saltMap := cache.GlobalCache.FetchMulti([]string{id + "_Salt", id + "_Time"})
	if saltMap == nil {
		return nil
	}
	ans := &Salt{
		Id: id,
	}
	if salt, ok := saltMap[id+"_Salt"]; !ok || salt == "" {
		return nil
	} else {
		ans.Salt = salt
	}
	if saltTime, ok := saltMap[id+"_Time"]; !ok || saltTime == "" {
		return nil
	} else {
		ans.Time = saltTime
	}
	return ans
}

// 创建一个salt
// salt组成:
// 	salt_id: uuid生成的唯一id
// 	time: 当前时间
// 	salt: sha1算法hash的 当前时间格式化到 "2006-01-02 15:04" 格式的字符串 + 随机新uuid + 设定好的key值
func NewSalt() (*Salt, error) {
	timeStr := time.Now().UTC().Format(TIME_PARSE)
	timeNow, _ := time.Parse(TIME_PARSE, timeStr)
	salt := &Salt{
		Id:   uuid.New(),
		Time: timeNow.UTC().Format(TIME_PARSE),
	}
	newUUID := uuid.New()
	tempSaltStr := timeStr + newUUID + PROVATE_KEY
	newHash := sha1.New()
	newHash.Write([]byte(tempSaltStr))
	salt.Salt = fmt.Sprintf("%x", newHash.Sum([]byte("")))
	if err := saveSalt(salt); err != nil {
		return nil, err
	}
	return salt, nil
}

// 依靠盐生成第二步验证数据
// 生成验证盐值
// 拿加密的 salt盐值字符串 + 输入时间格式化到 "2006-01-02 15:04" 格式的字符串 + 内部私有key 进行sha1算法hash
func MakeAuthBySalt(salt *Salt, captDate string) string {
	newHash := sha1.New()
	newHash.Write([]byte(salt.Salt + captDate + PROVATE_KEY))
	return fmt.Sprintf("%x", newHash.Sum([]byte("")))
}

// 验证数据是否准确
func Captcha(a GetAuth) bool {
	// 取服务器中的盐值
	salt := getSalt(a.Id)
	if salt == nil {
		return false
	}
	// 生成验证正确值
	rightStr := MakeAuthBySalt(salt, salt.Time)
	// 比较
	return a.AuthStr == rightStr
}

// 验证返回
func BackData(a *AuthStatus, w http.ResponseWriter) {
	w.WriteHeader(a.StatusCode)
	w.Header().Add("content-type", "application/json")
	backByte, _ := json.MarshalIndent(a, "", "\t")
	w.Write(backByte)
}
