package config

import (
	constconfig "gitee.com/MUMU-DADA/OneWordsAuth/config/ConstConfig"
)

type Config struct {
	Id          uint   // 服务器id
	Name        string // 服务器名称
	Addr        string // 服务器监听地址
	Schemes     string // 服务器监听方式 http/https
	Debug       bool   // 是否启动debug
	LogPath     string // 日志保存文件路径
	logLevel    string // 记录日志等级
	CacheSwitch string // 使用何种缓存引擎 可用 default/redis default为默认内存缓存
}

func (c *Config) GetLogLevel() string {
	return c.logLevel
}

// 初始化
func (c *Config) Init() {
	if c.Name == "" {
		c.Name = constconfig.SERVER_NAME
	}
	if c.Addr == "" {
		c.Addr = constconfig.ADDR
	}
	switch c.Schemes {
	case "http":
	case "https":
	default:
		c.Schemes = constconfig.SCHEMES
	}
	if c.LogPath == "" {
		c.LogPath = constconfig.LOG_PATH
	}
	if c.Debug {
		c.logLevel = constconfig.LOG_LEVEL
	} else {
		c.logLevel = "info"
	}
	if c.CacheSwitch == "" {
		c.CacheSwitch = constconfig.CACHE_SWITCH
	}
}

// 默认初始化
func (c *Config) Default() {
	c.Debug = true
	c.Init()
}
