module gitee.com/MUMU-DADA/OneWordsAuth

go 1.16

require (
	github.com/BurntSushi/toml v1.0.0 // indirect
	github.com/faabiosr/cachego v0.16.3
	github.com/go-basic/uuid v1.0.0
	github.com/gorilla/mux v1.8.0
	github.com/natefinch/lumberjack v2.0.0+incompatible
	go.uber.org/zap v1.21.0
	gopkg.in/natefinch/lumberjack.v2 v2.0.0 // indirect
	gopkg.in/redis.v4 v4.2.4
)
