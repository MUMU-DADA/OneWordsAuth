package constconfig

const (
	SERVER_ID    uint   = 0                          // 默认服务器id
	SERVER_NAME  string = "OneWordsAuthServer"       // 默认服务器名称
	ADDR         string = ":18809"                   // 默认服务器监听地址
	SCHEMES      string = "https"                    // 默认服务器监听方式 http/https
	LOG_PATH     string = "./OneWordsAuthServer.log" // 默认日志保存文件路径
	LOG_LEVEL    string = "debug"                    // 默认记录日志等级
	CACHE_SWITCH string = "default"                  // 默认缓存 default/redis
)
