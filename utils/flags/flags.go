package flags

import (
	"flag"

	"gitee.com/MUMU-DADA/OneWordsAuth/config"
	constconfig "gitee.com/MUMU-DADA/OneWordsAuth/config/ConstConfig"
)

func parseFlags() *config.Config {
	ans := &config.Config{}
	flag.UintVar(&ans.Id, "id", constconfig.SERVER_ID, "服务器id")
	flag.StringVar(&ans.Name, "name", constconfig.SERVER_NAME, "服务器名称")
	flag.StringVar(&ans.Addr, "addr", constconfig.ADDR, "服务器监听地址")
	flag.StringVar(&ans.Schemes, "schemes", constconfig.SCHEMES, "服务器监听方式 可选 http/https")
	flag.BoolVar(&ans.Debug, "debug", false, "是否启动debug")
	flag.StringVar(&ans.LogPath, "log_path", constconfig.LOG_PATH, "日志保存文件路径")
	flag.StringVar(&ans.CacheSwitch, "CacheSW", constconfig.CACHE_SWITCH, "使用何种缓存引擎 可用 default/redis default为默认内存缓存")
	flag.Parse()
	ans.Init()
	return ans
}

func InitFlags() *config.Config {
	return parseFlags()
}
