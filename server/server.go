package main

import (
	"flag"

	"gitee.com/MUMU-DADA/OneWordsAuth/pkgs/logs"
	"gitee.com/MUMU-DADA/OneWordsAuth/server/module"
	"gitee.com/MUMU-DADA/OneWordsAuth/utils/flags"
)

var (
	DEBUG bool
)

func initFlag() {
	flag.BoolVar(&DEBUG, "debug", false, "是否启动debug模式")
	flag.Parse()
}

func main() {
	// 初始化输入参数
	f := flags.InitFlags()
	// 生成服务端实例
	s := module.NewServer(f)
	// 启动服务端实例
	if err := s.Run(); err != nil {
		logs.L.Error(err.Error())
		return
	}
}
