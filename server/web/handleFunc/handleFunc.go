package handleFunc

import (
	"encoding/json"
	"io/ioutil"
	"net/http"

	DA "gitee.com/MUMU-DADA/OneWordsAuth/auth/DefaultAlgorithm"
)

// 获取实时盐
func HandleGetRealTimeCode(w http.ResponseWriter, r *http.Request) {
	salt, err := DA.NewSalt()
	if err != nil {
		return
	}
	saltJsonByte, err := json.Marshal(salt)
	if err != nil {
		return
	}
	w.Header().Add("content-type", "application/json")
	w.Write(saltJsonByte)
}

// 验证
func HandlerAuth(w http.ResponseWriter, r *http.Request) {
	// 取数据
	bodyByte, err := ioutil.ReadAll(r.Body)
	if err != nil {
		back := &DA.AuthStatus{
			StatusCode: http.StatusBadRequest,
			Msg:        "请求错误 " + err.Error(),
		}
		DA.BackData(back, w)
		return
	}
	var tempAuth DA.GetAuth
	if err := json.Unmarshal(bodyByte, &tempAuth); err != nil {
		back := &DA.AuthStatus{
			StatusCode: http.StatusBadRequest,
			Msg:        "请求body json解析错误 " + err.Error(),
		}
		DA.BackData(back, w)
		return
	}
	// 验证数据
	capt := DA.Captcha(tempAuth)
	if !capt {
		back := &DA.AuthStatus{
			StatusCode: http.StatusForbidden,
			Msg:        "验证失败",
		}
		DA.BackData(back, w)
		return
	}
	back := &DA.AuthStatus{
		StatusCode: http.StatusOK,
		Msg:        "ok",
	}
	DA.BackData(back, w)
}
