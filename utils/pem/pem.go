package pem

import (
	"crypto/rand"
	"crypto/rsa"
	"crypto/x509"
	"crypto/x509/pkix"
	"encoding/pem"
	"math/big"
	"net"
	"os"
	"time"

	"gitee.com/MUMU-DADA/OneWordsAuth/utils/files"
)

const (
	PROVATE_KEY_FILE_NAME string = "private.pem" // 私钥文件名
	PUBLIC_KEY_FILE_NAME  string = "public.pem"  // 公钥文件名

	TLS_CERT_FILE_NAME string = "cert.pem"     // 证书cert文件名
	TLS_KEY_FILE_NAME  string = "key.pem"      //  证书key文件名
	TLS_ORG            string = "MUMU-DADA"    // 证书组织
	TLS_UNIT           string = "OneWordsAuth" //  证书部门
	TLS_COMMON_NAME    string = "OneWordsAuth" //  证书common name
	TLS_URL            string = "127.0.0.1"    //  证书链接
)

// 生成tls证书
func MakeKey() error {
	max := new(big.Int).Lsh(big.NewInt(1), 128)
	serialNumber, err := rand.Int(rand.Reader, max)
	if err != nil {
		return err
	}
	subject := pkix.Name{
		Organization:       []string{TLS_ORG},
		OrganizationalUnit: []string{TLS_UNIT},
		CommonName:         TLS_COMMON_NAME,
	}

	rootTemplate := x509.Certificate{
		SerialNumber: serialNumber,
		Subject:      subject,
		NotBefore:    time.Now(),
		NotAfter:     time.Now().Add(365 * time.Hour),
		KeyUsage:     x509.KeyUsageDigitalSignature,
		ExtKeyUsage:  []x509.ExtKeyUsage{x509.ExtKeyUsageServerAuth},
		IPAddresses:  []net.IP{net.ParseIP(TLS_URL)},
	}
	pk, err := rsa.GenerateKey(rand.Reader, 2048)
	if err != nil {
		return err
	}
	derBytes, err := x509.CreateCertificate(rand.Reader, &rootTemplate, &rootTemplate, &pk.PublicKey, pk)
	if err != nil {
		return err
	}
	certOut, err := os.Create(TLS_CERT_FILE_NAME)
	if err != nil {
		return err
	}
	if err := pem.Encode(certOut, &pem.Block{Type: "CERTIFICATE", Bytes: derBytes}); err != nil {
		return err
	}

	certOut.Close()

	keyOut, err := os.Create(TLS_KEY_FILE_NAME)
	if err != nil {
		return err
	}
	if err := pem.Encode(keyOut, &pem.Block{Type: "RSA PRIVATE KEY", Bytes: x509.MarshalPKCS1PrivateKey(pk)}); err != nil {
		return err
	}
	keyOut.Close()
	return nil
}

//RSA公钥私钥产生
func GenRsaKey(bits int) error {
	// 生成私钥文件
	privateKey, err := rsa.GenerateKey(rand.Reader, bits)
	if err != nil {
		return err
	}
	derStream := x509.MarshalPKCS1PrivateKey(privateKey)
	block := &pem.Block{
		Type:  "RSA PRIVATE KEY",
		Bytes: derStream,
	}
	file, err := os.Create(PROVATE_KEY_FILE_NAME)
	if err != nil {
		return err
	}
	err = pem.Encode(file, block)
	if err != nil {
		return err
	}
	// 生成公钥文件
	publicKey := &privateKey.PublicKey
	derPkix, err := x509.MarshalPKIXPublicKey(publicKey)
	if err != nil {
		return err
	}
	block = &pem.Block{
		Type:  "PUBLIC KEY",
		Bytes: derPkix,
	}
	file, err = os.Create(PUBLIC_KEY_FILE_NAME)
	if err != nil {
		return err
	}
	err = pem.Encode(file, block)
	if err != nil {
		return err
	}
	return nil
}

// 检查并生成公私钥
func CheckPemKey() error {
	exist1, err := files.FileExists(TLS_CERT_FILE_NAME)
	if err != nil {
		return err
	}
	exist2, err := files.FileExists(TLS_KEY_FILE_NAME)
	if err != nil {
		return err
	}
	if !exist1 || !exist2 {
		os.Remove(TLS_CERT_FILE_NAME)
		os.Remove(TLS_KEY_FILE_NAME)
		if err := MakeKey(); err != nil {
			return err
		}
	}
	return nil
}
