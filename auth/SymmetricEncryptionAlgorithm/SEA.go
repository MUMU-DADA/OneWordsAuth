package SEA

var (
	AllSEAEngineMap map[string]ISEA = make(map[string]ISEA) // 保存注册的所有非对称加密算法引擎
)

// 通用非对称加密算法接口
type ISEA interface{}

// 初始化并注册所有引擎
func init() {}
