package module

import (
	"fmt"
	"net/http"

	"gitee.com/MUMU-DADA/OneWordsAuth/config"
	constconfig "gitee.com/MUMU-DADA/OneWordsAuth/config/ConstConfig"
	"gitee.com/MUMU-DADA/OneWordsAuth/pkgs/cache"
	"gitee.com/MUMU-DADA/OneWordsAuth/pkgs/logs"
	"gitee.com/MUMU-DADA/OneWordsAuth/server/web"
	"gitee.com/MUMU-DADA/OneWordsAuth/utils/pem"
)

// 服务结构体
type Server struct {
	Id     uint
	Name   string
	Web    *http.Server
	Config *config.Config
}

// 新建一个默认服务实例
func getDefaultServer() *Server {
	ans := &Server{
		constconfig.SERVER_ID,
		constconfig.SERVER_NAME,
		web.InitWeb(constconfig.ADDR, constconfig.SCHEMES),
		&config.Config{},
	}
	ans.Config.Default()
	logs.InitLog(constconfig.LOG_PATH, constconfig.LOG_LEVEL, constconfig.SERVER_NAME, constconfig.SERVER_ID)
	return ans
}

// 新建一个服务实例
func NewServer(cfg ...*config.Config) *Server {
	if cfg != nil || len(cfg) == 0 || cfg[0] == nil {
		return getDefaultServer()
	} else {
		logs.InitLog(cfg[0].LogPath, cfg[0].GetLogLevel(), cfg[0].Name, cfg[0].Id)
		return &Server{
			cfg[0].Id,
			cfg[0].Name,
			web.InitWeb(cfg[0].Addr, cfg[0].Schemes),
			cfg[0],
		}
	}
}

// 启动
func (s *Server) Run() error {
	// 开始循环监听本地缓存
	cache.Init(s.Config.CacheSwitch)
	switch s.Config.Schemes {
	case "http":
		logs.L.Info(fmt.Sprintf("id: %v 名称: %v 开始监听 %s://%s", s.Id, s.Name, s.Config.Schemes, s.Config.Addr))
		if err := s.Web.ListenAndServe(); err != nil {
			logs.L.Error(err.Error())
			return err
		}
	case "https":
		logs.L.Info(fmt.Sprintf("id: %v 名称: %v 开始监听 %s://%s", s.Id, s.Name, s.Config.Schemes, s.Config.Addr))
		if err := s.Web.ListenAndServeTLS(pem.TLS_CERT_FILE_NAME, pem.TLS_KEY_FILE_NAME); err != nil {
			logs.L.Error(err.Error())
			return err
		}
	default:
		err := fmt.Errorf("设置监听方式不为 http/https")
		logs.L.Error(err.Error())
		return err
	}
	return nil
}
