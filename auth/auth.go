package auth

import (
	AEA "gitee.com/MUMU-DADA/OneWordsAuth/auth/AsymmetricEncryptionAlgorithm"
	DA "gitee.com/MUMU-DADA/OneWordsAuth/auth/DefaultAlgorithm"
	LHA "gitee.com/MUMU-DADA/OneWordsAuth/auth/LinearHashAlgorithm"
	SEA "gitee.com/MUMU-DADA/OneWordsAuth/auth/SymmetricEncryptionAlgorithm"
	"gitee.com/MUMU-DADA/OneWordsAuth/config"
)

var Auth *auth = &auth{}

type auth struct {
	DefaultEngine *DA.DefaultEngine   // 默认引擎
	AEAEngines    map[string]AEA.IAEA // 保存注册的所有对称加密算法引擎
	LHAEngines    map[string]LHA.ILHA // 保存注册的所有线性散列算法引擎
	SEAEngines    map[string]SEA.ISEA // 保存注册的所有非对称加密算法引擎
}

// 获取默认引擎
func (a *auth) GetDefaultEngine(cfg *config.Config) *DA.DefaultEngine {
	return a.DefaultEngine
}

// 将所有初始化所有引擎载入
func init() {
	Auth.DefaultEngine = &DA.DefaultEngine{}
	Auth.AEAEngines = AEA.AllAEAEngineMap
	Auth.LHAEngines = LHA.AllLHAEngineMap
	Auth.SEAEngines = SEA.AllSEAEngineMap
}
