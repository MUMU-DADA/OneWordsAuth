package router

import (
	"gitee.com/MUMU-DADA/OneWordsAuth/server/web/handleFunc"
	"github.com/gorilla/mux"
)

func InitRouter(schemes string) *mux.Router {
	routerlist := mux.NewRouter()
	routerlist.HandleFunc("/get_real_time_code", handleFunc.HandleGetRealTimeCode).Methods("GET").Schemes(schemes)
	routerlist.HandleFunc("/auth", handleFunc.HandlerAuth).Methods("POST").Schemes(schemes)
	return routerlist
}
